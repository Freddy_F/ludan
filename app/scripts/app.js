'use strict';

/**
 * @ngdoc overview
 * @name ludanApp
 * @description
 * # ludanApp
 *
 * Main module of the application.
 */
angular
  .module('ludanApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/baccarat', {
        templateUrl: 'views/baccarat.html',
        controller: 'AboutCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
