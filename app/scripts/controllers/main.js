'use strict';

/**
 * @ngdoc function
 * @name ludanApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ludanApp
 */
angular.module('ludanApp')
  .controller('MainCtrl', function ($scope,$http) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    //zhupan
    $scope.zhupan=[];
    for( var l=0;l<20;l++){
    	var row=[];
			for(var r=0;r<6;r++){
				row[r]={p:[l,r],v:9,bp:false,pp:false};
			}    	
			$scope.zhupan[l]=row;
    }
    //dalu
    $scope.daluTable=[];
    for( var l=0;l<34;l++){
    	var row=[];
			for(var r=0;r<6;r++){
                var cell={x:l,y:r,perl:{}};
				row[r]=cell;
			}    	
			$scope.daluTable[l]=row;
    }
    $scope.curCell=$scope.daluTable[0][0];

    $scope.daluPerl=[];
    for( var l=0;l<34;l++){
    	var row=[];
    	$scope.daluPerl[l]=row;
    }
    $scope.curDaluPerl=$scope.daluPerl[0][0]={x:0,y:0,v:undefined,he:0};

    //dayan
    $scope.dayanTable=[];
    for( var l=0;l<68;l++){
        var row=[];
            for(var r=0;r<6;r++){
                var cell={x:l,y:r,perl:{}};
                row[r]=cell;
            }       
            $scope.dayanTable[l]=row;
    }
    $scope.dayanCell=$scope.daluTable[0][0];

    $scope.dayanPerl=[];
    for( var l=0;l<100;l++){
    	var row=[];
	   $scope.dayanPerl[l]=row;
    }
    $scope.curDayanPerl=$scope.dayanPerl[0][0]={x:0,y:0,v:undefined};

    //xiaolu
    $scope.xiaoTable=[];
    for( var l=0;l<34;l++){
        var row=[];
            for(var r=0;r<6;r++){
                var cell={x:l,y:r,perl:{}};
                row[r]=cell;
            }       
            $scope.xiaoTable[l]=row;
    }
    $scope.xiaoCell=$scope.xiaoTable[0][0];

    $scope.xiaoPerl=[];
    for( var l=0;l<100;l++){
        var row=[];
       $scope.xiaoPerl[l]=row;
    }
    $scope.curxiaoPerl=$scope.xiaoPerl[0][0]={x:0,y:0,v:undefined};

    //zhanglang
    $scope.zhangTable=[];
    for( var l=0;l<34;l++){
        var row=[];
            for(var r=0;r<6;r++){
                var cell={x:l,y:r,perl:{}};
                row[r]=cell;
            }       
            $scope.zhangTable[l]=row;
    }
    $scope.zhangCell=$scope.zhangTable[0][0];

    $scope.zhangPerl=[];
    for( var l=0;l<100;l++){
        var row=[];
       $scope.zhangPerl[l]=row;
    }
    $scope.curzhangPerl=$scope.zhangPerl[0][0]={x:0,y:0,v:undefined};


    $scope.zhupanp=[0,0];
    $scope.daluCursor={x:0,y:0};
    $scope.daluTableCursor={x:0,y:0};
    $scope.add=function(won,bp,pp){//won:0庄1闲2和，bp,pp:true对false没对
    	console.log(won,bp,pp);
  		$scope.zhupan[$scope.zhupanp[0]][$scope.zhupanp[1]].v=won;
  		$scope.zhupan[$scope.zhupanp[0]][$scope.zhupanp[1]].bp=bp;
  		$scope.zhupan[$scope.zhupanp[0]][$scope.zhupanp[1]].pp=pp;
    	if($scope.zhupanp[1]<=4){
    		$scope.zhupanp[1]=$scope.zhupanp[1]*1+1;
    	}else{
    		$scope.zhupanp[1]=0;
    		$scope.zhupanp[0]=$scope.zhupanp[0]*1+1;
    	}

        $scope.addDalu(won);
    };
    $scope.addDalu=function(won){   
        if(won===0||won===1){
            if($scope.curDaluPerl.v===undefined){
                $scope.curDaluPerl.v=won;
            }
            else if($scope.curDaluPerl.v===won){
                
                $scope.curDaluPerl=$scope.daluPerl[$scope.curDaluPerl.x][$scope.curDaluPerl.y+1]={
                    x:$scope.curDaluPerl.x,
                    y:$scope.curDaluPerl.y+1,
                    v:won,
                    he:0
                };
            }else{
                 $scope.curDaluPerl=$scope.daluPerl[$scope.curDaluPerl.x+1][0]={
                    x:$scope.curDaluPerl.x+1,
                    y:0,
                    v:won,
                    he:0
                };
            }
            
            if($scope.curDaluPerl.y>5||($scope.curCell.y<5&&$scope.curDaluPerl.y>=$scope.curCell.y&&$scope.daluTable[$scope.curCell.x][$scope.curCell.y+1].perl.v>-1)){// next down taken or touch bottom
                $scope.daluTable[$scope.curCell.x+1][$scope.curCell.y].perl=$scope.curDaluPerl;
                $scope.curCell=$scope.daluTable[$scope.curCell.x+1][$scope.curCell.y];
            }else{
                $scope.daluTable[$scope.curDaluPerl.x][$scope.curDaluPerl.y].perl=$scope.curDaluPerl;
                $scope.curCell=$scope.daluTable[$scope.curDaluPerl.x][$scope.curDaluPerl.y];
            }

          //handle dayan  
          if($scope.curDaluPerl.x>=1&&$scope.curDaluPerl.y>=1||$scope.curDaluPerl.x>=2){
            $scope.addDayan($scope.curDaluPerl);
            }  
            //handle xiao  
          if($scope.curDaluPerl.x>=2&&$scope.curDaluPerl.y>=1||$scope.curDaluPerl.x>=3){
            $scope.addxiao($scope.curDaluPerl);
            }  
            //handle zhanglang 
          if($scope.curDaluPerl.x>=3&&$scope.curDaluPerl.y>=1||$scope.curDaluPerl.x>=4){
            $scope.addzhang($scope.curDaluPerl);
            }  
        }    
        

        if(won===2){
            $scope.curDaluPerl.he++;
        }

        
    };
    
    $scope.addDayan=function(curDaluPerl) {
        var curV ="";
        // console.log(curDaluPerl,$scope.daluPerl[curDaluPerl.x-1][curDaluPerl.y]);
        if($scope.curDaluPerl.y===0){
            if($scope.daluPerl[curDaluPerl.x-1].length===$scope.daluPerl[curDaluPerl.x-2].length){
                curV= "red";
            }else{
                curV="blue";
            }
        }
        if($scope.curDaluPerl.y>0){
            if($scope.daluPerl[curDaluPerl.x-1][curDaluPerl.y]){
                curV="red";
            }else{
                if($scope.daluPerl[curDaluPerl.x-1][curDaluPerl.y-1]){
                    curV="blue";
                }else{
                    curV="red";
                }
            }
        }
        // console.log(curV);
        if($scope.curDayanPerl.v===undefined){
            $scope.curDayanPerl.v=curV;
        }else if($scope.curDayanPerl.v===curV){
            $scope.curDayanPerl=$scope.dayanPerl[$scope.curDayanPerl.x][$scope.curDayanPerl.y+1]={
                    x:$scope.curDayanPerl.x,
                    y:$scope.curDayanPerl.y+1,
                    v:curV};

        }else{
            $scope.curDayanPerl=$scope.dayanPerl[$scope.curDayanPerl.x+1][0]={
                    x:$scope.curDayanPerl.x+1,
                    y:0,
                    v:curV};
        }

        if($scope.curDayanPerl.y>5||($scope.dayanCell.y<5&&$scope.curDayanPerl.y>=$scope.dayanCell.y&&$scope.dayanTable[$scope.dayanCell.x][$scope.dayanCell.y+1].perl.v)){// next down taken or touch bottom
                $scope.dayanTable[$scope.dayanCell.x+1][$scope.dayanCell.y].perl=$scope.curDayanPerl;
                $scope.dayanCell=$scope.dayanTable[$scope.dayanCell.x+1][$scope.dayanCell.y];
            }else{
                $scope.dayanTable[$scope.curDayanPerl.x][$scope.curDayanPerl.y].perl=$scope.curDayanPerl;
                $scope.dayanCell=$scope.dayanTable[$scope.curDayanPerl.x][$scope.curDayanPerl.y];
            }
            // console.log($scope.dayanPerl);
            // console.log($scope.dayanTable);
    };   

    $scope.addxiao=function(curDaluPerl) {
        var curV ="";
        // console.log(curDaluPerl,$scope.daluPerl[curDaluPerl.x-1][curDaluPerl.y]);
        if($scope.curDaluPerl.y===0){
            if($scope.daluPerl[curDaluPerl.x-1].length===$scope.daluPerl[curDaluPerl.x-3].length){
                curV= "red";
            }else{
                curV="blue";
            }
        }
        if($scope.curDaluPerl.y>0){
            if($scope.daluPerl[curDaluPerl.x-2][curDaluPerl.y]){
                curV="red";
            }else{
                if($scope.daluPerl[curDaluPerl.x-2][curDaluPerl.y-1]){
                    curV="blue";
                }else{
                    curV="red";
                }
            }
        }
        // console.log(curV);
        if($scope.curxiaoPerl.v===undefined){
            $scope.curxiaoPerl.v=curV;
        }else if($scope.curxiaoPerl.v===curV){
            $scope.curxiaoPerl=$scope.xiaoPerl[$scope.curxiaoPerl.x][$scope.curxiaoPerl.y+1]={
                    x:$scope.curxiaoPerl.x,
                    y:$scope.curxiaoPerl.y+1,
                    v:curV};

        }else{
            $scope.curxiaoPerl=$scope.xiaoPerl[$scope.curxiaoPerl.x+1][0]={
                    x:$scope.curxiaoPerl.x+1,
                    y:0,
                    v:curV};
        }

        if($scope.curxiaoPerl.y>5||($scope.xiaoCell.y<5&&$scope.curxiaoPerl.y>=$scope.xiaoCell.y&&$scope.xiaoTable[$scope.xiaoCell.x][$scope.xiaoCell.y+1].perl.v)){// next down taken or touch bottom
                $scope.xiaoTable[$scope.xiaoCell.x+1][$scope.xiaoCell.y].perl=$scope.curxiaoPerl;
                $scope.xiaoCell=$scope.xiaoTable[$scope.xiaoCell.x+1][$scope.xiaoCell.y];
            }else{
                $scope.xiaoTable[$scope.curxiaoPerl.x][$scope.curxiaoPerl.y].perl=$scope.curxiaoPerl;
                $scope.xiaoCell=$scope.xiaoTable[$scope.curxiaoPerl.x][$scope.curxiaoPerl.y];
            }
            // console.log($scope.xiaoPerl);
            // console.log($scope.xiaoTable);
    };
    $scope.addzhang=function(curDaluPerl) {
        var curV ="";
        // console.log(curDaluPerl,$scope.daluPerl[curDaluPerl.x-1][curDaluPerl.y]);
        if($scope.curDaluPerl.y===0){
            if($scope.daluPerl[curDaluPerl.x-1].length===$scope.daluPerl[curDaluPerl.x-4].length){
                curV= "red";
            }else{
                curV="blue";
            }
        }
        if($scope.curDaluPerl.y>0){
            if($scope.daluPerl[curDaluPerl.x-3][curDaluPerl.y]){
                curV="red";
            }else{
                if($scope.daluPerl[curDaluPerl.x-3][curDaluPerl.y-1]){
                    curV="blue";
                }else{
                    curV="red";
                }
            }
        }
        // console.log(curV);
        if($scope.curzhangPerl.v===undefined){
            $scope.curzhangPerl.v=curV;
        }else if($scope.curzhangPerl.v===curV){
            $scope.curzhangPerl=$scope.zhangPerl[$scope.curzhangPerl.x][$scope.curzhangPerl.y+1]={
                    x:$scope.curzhangPerl.x,
                    y:$scope.curzhangPerl.y+1,
                    v:curV};

        }else{
            $scope.curzhangPerl=$scope.zhangPerl[$scope.curzhangPerl.x+1][0]={
                    x:$scope.curzhangPerl.x+1,
                    y:0,
                    v:curV};
        }

        if($scope.curzhangPerl.y>5||($scope.zhangCell.y<5&&$scope.curzhangPerl.y>=$scope.zhangCell.y&&$scope.zhangTable[$scope.zhangCell.x][$scope.zhangCell.y+1].perl.v)){// next down taken or touch bottom
                $scope.zhangTable[$scope.zhangCell.x+1][$scope.zhangCell.y].perl=$scope.curzhangPerl;
                $scope.zhangCell=$scope.zhangTable[$scope.zhangCell.x+1][$scope.zhangCell.y];
            }else{
                $scope.zhangTable[$scope.curzhangPerl.x][$scope.curzhangPerl.y].perl=$scope.curzhangPerl;
                $scope.zhangCell=$scope.zhangTable[$scope.curzhangPerl.x][$scope.curzhangPerl.y];
            }
            // console.log($scope.zhangPerl);
            // console.log($scope.zhangTable);
    };



    $scope.won =undefined;
    $scope.bp=false;
    $scope.pp=false;
    $scope.deskId="#16:1";
    $scope.serial=1;
    var auth="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJCb2IiLCJpYXQiOjE0NDUzMzExNDl9.Rr4x4-rzETOIUH49TQq69wfHwE3_yLguQZGD5ryijrA=";
     var dict={
            "banker":"庄赢",
            "player":"闲赢",
            "tie":"和"
                    }; 
        var resultDict={
            "banker":"BankerWon",
            "player":"PlayerWon",
            "tie":"Tied"
                    }; 
    var addDict={
        "banker":0,
        "player":1,
        "tie":2
    };
    $scope.submit=function(){
       
        var str =""+dict[$scope.won]+","+$scope.bp+","+$scope.pp;
        // console.log($scope.won,$scope.bp,$scope.pp);
        console.log(str);
        // $http({
        //         method:"GET",
        //         url:"http://localhost:8080/private/rest/user/Bob3",
        //         headers:{
        //             Authorization:auth
        //         }
        //     }).success(function(data){ console.log(data)});
        $scope.addBaccarat();

    };
    $scope.addShuffle=function(){
        $http({
            method:"POST",
            url:"http://localhost:8080/private/rest/desk/shuffle",
            headers:{
                    "Authorization":auth,
                    "Content-Type": "application/x-www-form-urlencoded",
                    "dataType":"hs"
                },
            data:{
                deskId:$scope.deskId,
                shuffleCode:"testAddShuffle6"
            },
            transformRequest:function(obj){
                var str=[];
                for(var p in obj){
                    str.push(encodeURIComponent(p)+"="+encodeURIComponent(obj[p]));
                }
                return str.join("&");
            }
        }).success(function(data){
            console.log(data);
            $scope.data=data;
            $scope.currentSerial = $scope.data.name+"20151109"+$scope.data.currentShuffle.id+$scope.serialStr($scope.serial);
        });
    };
  
    $scope.addBaccarat=function(){
        if($scope.won===undefined){
            alert("no valid result");
            return;
        }
        $scope.currentSerial = $scope.data.name+"20151109"+$scope.data.currentShuffle.id+$scope.serialStr($scope.serial);
        $http({
            method:"POST",
            url:"http://localhost:8080/private/rest/desk/baccarat?shuffleId="+$scope.data.currentShuffle.id.substr(1,$scope.data.currentShuffle.id.length-1),
            headers:{
                    Authorization:auth
                },
            data:{
                  "serial": $scope.currentSerial,
                  "betResult": resultDict[$scope.won],
                  "banker": {
                    "cards": [
                      "SpadesAce",
                      "ClubsAce",
                      "ClubsFive"
                    ]
                  },
                  "player": {
                    "cards": [
                      "SpadesAce",
                      "ClubsAce",
                      "ClubsFive"
                    ]
                  },
                  "isBankerPair": $scope.bp,
                  "isPlayerPair": $scope.pp
                }
        }).success(function(data){
            console.log(data);
            //handle ludan;
            $scope.add(addDict[$scope.won],$scope.bp,$scope.pp);
            $scope.won =undefined;
            $scope.bp=false;
            $scope.pp=false;
        });
        $scope.serial++;
    };

    $scope.serialStr=function(num){
        if(num<10){
            return "00"+num;
        }
        if(num<100){
            return "0"+num;
        }
    };
  });
